<?php  
//Incluyo el archivo de conexion y de configuracion necesario
require('conexion.php');
	
    //Llamo a la conexion a la bbdd
    $conexion=Conexion::$conexion;

    //Creo variables para realizar un resumen de las entidades creadas
    //Esto es completamente opcional, solo se emplea para realizar el resumen
    $vectorNumPropiedades=[];
    $vectorNombreTablas=[];
    $vectorNombreFicheros=[];
    $numPropiedades=0;

	//Creo el directorio en el caso de no existir
	if(!is_dir($dir)){
		mkdir($dir);
	}

    //Creo una consulta para recorrer TODAS las tablas de la base de datos
	$sqlTablas="SHOW TABLES";
	$consulta = $conexion->query($sqlTablas);
    //Creo un bucle para ir recorriendo tabla tras tabla
	while($tabla=$consulta->fetch_array()) {
		//Aqui esta el nombre de la tabla
		$nombreTabla=$tabla[0];

        //Esto es completamente opcional, solo se emplea para realizar el resumen
        //Almaceno info sobre la tabla que estoy creando
        $vectorNombreTablas[]=$nombreTabla;

        //Consulta para extraer todos los campos de la tabla en cuestrion
		$sqlTabla="SELECT * FROM $nombreTabla";
		$consultaTabla = $conexion->query($sqlTabla);
		//En la FILA, esta cada uno de los campos de la tabla
		$fila=$consultaTabla->fetch_fields();

		//Creo el nombre del fichero
		$fichero=$dir.'/class.'.$nombreTabla.'.php';
		$vectorNombreFicheros[]=$fichero;

        //Abro el fichero para crear la entidad
        $f=fopen($fichero, 'w');

		//Escribo lo inicial de la Class
    	fwrite($f, "<?php\r\nClass ".ucfirst($nombreTabla));
    	fwrite($f, '{');

    	fwrite($f,"\r\n\r\n");
    	fwrite($f, "\t////////////////////////////\r\n");
        fwrite($f, "\t//SE GENERAN LAS PROPIEDADES\r\n");
        fwrite($f, "\t////////////////////////////\r\n");
        fwrite($f, "\r\n");

        //Bucle para generar las propiedades privadas
        $numPropiedades=0;
        foreach ($fila as $campo) {
            fwrite($f, "\tprivate \$$campo->name");
            fwrite($f, ';');
            fwrite($f, "\r\n");
            //printf("Tabla:            %s\n",   $campo->table);
            //printf("Longitud máx.:    %d\n",   $campo->max_length);
            //printf("Longitud:         %d\n",   $campo->length);
            //printf("Nº conj. caract.: %d\n",   $campo->charsetnr);
            //printf("Banderas:         %d\n",   $campo->flags);
            //printf("Tipo:             %d\n\n", $campo->type);
            
            //Esto es completamente opcional, solo se emplea para realizar el resumen
            //incremento el numero de propiedades
            $numPropiedades++;
        }
        //Esto es completamente opcional, solo se emplea para realizar el resumen
        $vectorNumPropiedades[]=$numPropiedades;

        //Creo lo que seria el constructor
        fwrite($f, "\r\n");
        fwrite($f, "\t///////////////////////////\r\n");
        fwrite($f, "\t//SE GENERAN EL CONSTRUCTOR\r\n");
        fwrite($f, "\t///////////////////////////\r\n");
        fwrite($f, "\r\n");

        fwrite($f, "\tpublic function __construct(\$fila=NULL)");
        fwrite($f, '{');
        fwrite($f, "\r\n");
        fwrite($f, "\t\tif(\$fila!=NULL){\r\n");
        //por cada campo, lo añado al constructor
        foreach ($fila as $campo) {

            fwrite($f, "\t\t\t\$this->$campo->name=\$fila['$campo->name']");
            fwrite($f, ';');
            fwrite($f, "\r\n");
            
        }
        fwrite($f, "\t\t");
        fwrite($f, '}');
        fwrite($f, "\t");
        fwrite($f, "\r\n");
        fwrite($f, "\t");
        fwrite($f, '}');

        fwrite($f, "\r\n\r\n");
        fwrite($f, "\t////////////////////////\r\n");
        fwrite($f, "\t//SE GENERAN LOS GETTERS\r\n");
        fwrite($f, "\t////////////////////////\r\n");
        fwrite($f, "\r\n");

        //Creo un bucle para generar los getters
        foreach ($fila as $campo) {

            fwrite($f, "\tpublic function get".ucfirst($campo->name)."()");
            fwrite($f, '{');
            fwrite($f, "\r\n");
            fwrite($f, "\t\treturn \$this->$campo->name");
            fwrite($f, ';');
            fwrite($f, "\r\n");
            
            fwrite($f, "\t");
            fwrite($f, '}');
            fwrite($f, "\r\n\r\n");
        }
        
        fwrite($f, "\t////////////////////////\r\n");
        fwrite($f, "\t//SE GENERAN LOS SETTERS\r\n");
        fwrite($f, "\t////////////////////////\r\n");
        fwrite($f, "\r\n");

        //Creo un bucle para generar los setters
        foreach ($fila as $campo) {

            fwrite($f, "\tpublic function set".ucfirst($campo->name)."(\$$campo->name)");
            fwrite($f, '{');
            fwrite($f, "\r\n");
            fwrite($f, "\t\t\$this->$campo->name=\$$campo->name");
            fwrite($f, ';');
            fwrite($f, "\r\n");
            
            fwrite($f, "\t");
            fwrite($f, '}');
            fwrite($f, "\r\n\r\n");
        }
        
        //Finalizo la Class
        fwrite($f, '}');
        fwrite($f, '?>');

        

        //Cierro ficheros
        fclose($f);
       

    } //fin del bucle por cada tabla que encuentro en la base de datos


    //Creo el fichero para los includes
    //Fichero para los includes
    $ficheroIncludes=$dir.'/'.$dir.'.php';

    //Creo un archivo para los includes
    $finc=fopen($ficheroIncludes, 'w');
    fwrite($finc, "<?php\r\n");

    foreach ($vectorNombreFicheros as $fichero) {
        //Escribo el include en su sitio
        fwrite($finc, "\r\nrequire('$fichero');");
    }

    //Finalizo el archivo de include
    fwrite($finc, "\r\n\r\n");
    fwrite($finc, '?>');
    fclose($finc);

    //Muestro algo de informacion:
    //Esto es completamente opcional, pero por lo menos muestra lo que ha realizado el script
    echo '<h3>Entidades creadas en el directorio <a href="'.$dir.'">'.$dir.'</a></h3>';
    echo "<h4>Nombre de la entidad, y numero de propiedades, getters y setters generados:</h4>";
    foreach ($vectorNombreTablas as $key => $value) {
        echo '<b>'.ucfirst($value).'</b>';
        echo ' (';
        echo $vectorNumPropiedades[$key];
        echo ' propiedades, getters y setters)<br>';
    }
    echo '<br>';
    echo '<h4>Numero de entidades creadas: '.count($vectorNumPropiedades).'</h4>';
    echo '<h4>Numero de propiedades, getters y setters generados: '.array_sum($vectorNumPropiedades).'</h4>';

?>