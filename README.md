# README #

## VERSIONES ##

### Version 0.8 ###

* Añado una clase para la conexion a la base de datos
* Añado un gestor de entidades, con la clase EM

#### LISTADO DE METODOS: ####
* static function sacarMetodos($clase);
* static function insertarElemento($clase);
* static function borrarElemento($clase);
* static function borrarElementoPorId($clase, $id);
* static function obtenerElementoPorId($clase, $id);
* static function guardarElemento($clase);

### Version 0.9 ###
* Añado obtenerElementoPorId($clase, $id)
* Añado guardarElemento($clase)
* Modifico la clase insertarElemento, para que cuando el codigo de la entidad no esta rellenado, se inserta el autonumerico


## INFO generador de entidades: ##
* Configurar los datos en el archivo conexion.php
* Para generar las entidades, llamamos a "generator.php"

## INFO Entity Manager: ##
* Ver el archivo pruebas.php para ver ejemplos
