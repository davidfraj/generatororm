<?php  
require('conexion.php');
require('entidades/entidades.php');

//Clase para el Entity Manager
//LISTADO DE METODOS:
// static function sacarMetodos($clase);
// static function insertarElemento($clase); 
// static function borrarElemento($clase);
// static function borrarElementoPorId($clase, $id);
// static function obtenerElementoPorId($clase, $id);
// static function guardarElemento($clase);

class EM{

	// Metodo de prueba para sacar el listado de metodos
	static function sacarMetodos($clase){
		$metodos = get_class_methods($clase);

		foreach ($metodos as $n) {
		    echo "$n<br>";
		}
	} // Fin de sacarMetodos()

	// Le paso un objeto de la clase que sea, que tenga una entidad, y la inserta en la bbdd
	static function insertarElemento($clase){

		//Obtengo los metodos
		$metodos = get_class_methods($clase);

		//Obtengo el campo id
		foreach ($metodos as $i => $campo) {
			if($i==1){
				$campoId=lcfirst(substr($campo, 3));
			}
		}


		//Creo la consulta sql de forma dinamica
		$sql="INSERT INTO ".lcfirst(get_class($clase))." (";
		$campos='';
		$valores='';
		$i=0;

		if($clase->{'get'.$campoId}() > 0){

			foreach ($metodos as $campo) {
				if(substr($campo,0,3)=='get'){
			    	if($i>0){
			    		$campos.=", ";
			    		$valores.=", ";
			    	}
			    	$campos.=lcfirst(substr($campo, 3));
			    	$valores.="'";
			    	$valores.=$clase->{$campo}();
			    	$valores.="'";
					$i++;
				}
			}

		}else{

			foreach ($metodos as $campo) {
				if(substr($campo,0,3)=='get'){
					if($i>0){
				    	if($i>1){
				    		$campos.=", ";
				    		$valores.=", ";
				    	}
				    	$campos.=lcfirst(substr($campo, 3));
				    	$valores.="'";
				    	$valores.=$clase->{$campo}();
				    	$valores.="'";
			    	}
					$i++;
				}
			}

		}

		$sql.=$campos;
		$sql.=")VALUES(";
		$sql.=$valores;

		$sql.=")";
		
		// echo $sql;
		if(Conexion::$conexion->query($sql)){
			return true;
		}else{
			return false;
		}

	} // Fin de insertarElemento()

	// Le paso un objeto de la clase que sea, que tenga una entidad, y la borra de la bbdd
	static function borrarElemento($clase){

		//Obtengo los metodos
		$metodos = get_class_methods($clase);

		//Obtengo el campo id
		foreach ($metodos as $i => $campo) {
			if($i==1){
				$campoId=lcfirst(substr($campo, 3));
				$valorCampoId=$clase->{$campo}();
			}
		}

		//Creo la consulta sql de forma dinamica
		$sql="DELETE FROM ".lcfirst(get_class($clase))." WHERE $campoId=$valorCampoId";

		//echo $sql;
		if(Conexion::$conexion->query($sql)){
			return true;
		}else{
			return false;
		}

	} // Fin de borrarElemento()

	// Le paso un objeto de la clase que sea, que tenga una entidad, y el id, y la borra de la bbdd
	static function borrarElementoPorId($clase, $id){

		//Obtengo los metodos
		$metodos = get_class_methods($clase);

		//Obtengo el campo id
		foreach ($metodos as $i => $campo) {
			if($i==1){
				$campoId=lcfirst(substr($campo, 3));
			}
		}

		//Creo la consulta sql de forma dinamica
		$sql="DELETE FROM ".lcfirst(get_class($clase))." WHERE $campoId=$id";

		//echo $sql;
		if(Conexion::$conexion->query($sql)){
			return true;
		}else{
			return false;
		}

	} // Fin de borrarElementoPorId

	// Le paso un objeto de la clase que sea, que tenga una entidad y el id, y devuelve un objeto de esa entidad
	static function obtenerElementoPorId($clase, $id){

		//Obtengo los metodos
		$metodos = get_class_methods($clase);

		//Obtengo el campo id
		foreach ($metodos as $i => $campo) {
			if($i==1){
				$campoId=lcfirst(substr($campo, 3));
			}
		}

		//Creo la consulta sql de forma dinamica
		$sql="SELECT * FROM ".lcfirst(get_class($clase))." WHERE $campoId=$id";

		$consulta=Conexion::$conexion->query($sql);

		return new $clase($consulta->fetch_array());

	} // Fin de obtenerElementoPorId()

	// Le paso un objeto de la clase que sea, y actualiza sus datos en la bbdd
	static function guardarElemento($clase){
		
		//Obtengo los metodos
		$metodos = get_class_methods($clase);

		//Obtengo el campo id
		foreach ($metodos as $i => $campo) {
			if($i==1){
				$campoId=lcfirst(substr($campo, 3));
				$valorId=$clase->{$campo}();
			}
		}

		//Creo la consulta sql de forma dinamica
		$sql="UPDATE ".lcfirst(get_class($clase))." SET ";

		$i=0;
		foreach ($metodos as $campo) {
			if(substr($campo,0,3)=='get'){
		    	if($i>0){
		    		$sql.=", ";
		    	}
		    	$sql.=lcfirst(substr($campo, 3));
		    	$sql.="='";
		    	$sql.=$clase->{$campo}();
		    	$sql.="'";
			$i++;
			}
		}

		$sql.=" WHERE $campoId=$valorId";
		
		//echo $sql;
		if(Conexion::$conexion->query($sql)){
			return true;
		}else{
			return false;
		}
	
	} // Fin de guardarElemento()

}


?>